{{- define "kaniko-job-template" }}
---
apiVersion: batch/v1
kind: Job
metadata:
  name: {{ .Chart.Name }}
  namespace: jobs-kaniko
  annotations:
    "helm.sh/hook": pre-upgrade
    "helm.sh/hook-weight": "0"
    "helm.sh/hook-delete-policy": before-hook-creation
spec:
  backoffLimit: 1
  template:
    spec:
      restartPolicy: Never
      containers:
        - name: kaniko
          image: gcr.io/kaniko-project/executor:v1.7.0
          args:
            - --context={{ regexReplaceAll "^https?://" .Values.repo.location "git://" }}#refs/heads/{{ .Values.repo.branch }}
            - --destination={{ .Values.docker.location }}:{{ .Values.docker.tag }}
            - --dockerfile={{ .Values.repo.dockerfile }}
            - --cache=true
            - --cache-copy-layers
            - --snapshotMode=redo
            - --compressed-caching=false
            - --use-new-run
          volumeMounts:
            - name: kaniko-secret
              mountPath: /kaniko/.docker
          securityContext:
            runAsUser: 0
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
              - matchExpressions:
                  - key: kaniko
                    operator: Exists
      volumes:
        - name: kaniko-secret
          secret:
            secretName: dockercred
            items:
              - key: .dockerconfigjson
                path: config.json
{{- end }}

{{- define "retrieve-rollme-value" }}
{{ if or .Values.build .Values.redeploy }}
{{- randAlphaNum 12 | quote }}
{{ else }}
{{- dig "metadata" "annotations" "rollme" (randAlphaNum 12 | quote) (lookup "apps/v1" (default "Deployment" .kind) .Release.Namespace (default .Chart.Name .chart_name)) }}
{{ end }}
{{- end }}

{{- define "check-if-build-container" }}
{{ if .Values.build }}
{{- include "kaniko-job-template" . }}
{{ end }}
{{- end }}
