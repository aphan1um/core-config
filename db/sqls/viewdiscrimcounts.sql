CREATE MATERIALIZED VIEW IF NOT EXISTS viewdiscrimcounts AS
SELECT *
FROM crosstab('SELECT jobphrases.group_id as group_id, phrases.phrase_name as phrase_name, SUM(jobphrases.value) as value
    FROM jobphrases
    JOIN phrases USING (phrase_id)
    JOIN phrasetypes USING (phrasetype_id)
    WHERE NOT phrasetypes.phrasetype = ''softskill''
    GROUP BY group_id, phrase_name
    ORDER BY group_id, phrase_name') AS tbl(group_id int, "gender-implicit-masculine" real, "gender-implicit-feminine" real, "disability-general" real);