#!/bin/bash

until pg_isready -U postgres -d postgres -h "${POSTGRES_HOST}" 2>/dev/null
do
  echo "Postgres is unavailable - sleeping for 5 seconds"
  sleep 5
done

echo ">> Beginning database init..."

PGPASSWORD=${POSTGRES_PASSWORD} psql -v ON_ERROR_STOP=1 -U postgres -d postgres -h "${POSTGRES_HOST}" <<-EOSQL

-- DROP SCHEMA public CASCADE;
-- CREATE SCHEMA public;

SELECT 'DROP TABLE IF EXISTS ' || tablename || ' CASCADE;' FROM pg_tables WHERE schemaname = 'public' AND tablename not in ('jobs');
\gexec


CREATE EXTENSION IF NOT EXISTS tablefunc;


CREATE OR REPLACE FUNCTION update_modified_column_time()   
RETURNS TRIGGER AS
\$\$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
\$\$ LANGUAGE 'plpgsql';

CREATE TABLE infofirmtypes (
  firm_type VARCHAR (16) NOT NULL UNIQUE PRIMARY KEY
);

CREATE TABLE infoabrentities (
  entity_id VARCHAR (3) NOT NULL UNIQUE PRIMARY KEY,
  title VARCHAR (64) NOT NULL,
  firm_type VARCHAR (16),
  FOREIGN KEY (firm_type) REFERENCES infofirmtypes(firm_type) ON DELETE RESTRICT
);

CREATE TABLE infostates (
  state_code VARCHAR(8) NOT NULL UNIQUE PRIMARY KEY
);

CREATE TABLE infoareas (
  area_id SERIAL PRIMARY KEY,
  sa2_name VARCHAR(128) NOT NULL,
  sa3_name VARCHAR(128) NOT NULL,
  sa4_name VARCHAR(128) NOT NULL,
  state_name VARCHAR(8) NOT NULL,
  poa_code integer NOT NULL,
  ssc_name VARCHAR(64) NOT NULL,
  FOREIGN KEY (state_name) REFERENCES infostates(state_code) ON DELETE RESTRICT
);

CREATE TABLE infoanzsicdivision (
  division_id VARCHAR (1) NOT NULL UNIQUE PRIMARY KEY,
  division_name VARCHAR (64) NOT NULL
);

CREATE TABLE infoanzsic (
  anzsic_id INTEGER NOT NULL UNIQUE PRIMARY KEY,
  title VARCHAR (128) NOT NULL,
  level integer NOT NULL,
  division VARCHAR (1) NOT NULL,
  FOREIGN KEY (division) REFERENCES infoanzsicdivision(division_id) ON DELETE RESTRICT
);

CREATE OR REPLACE FUNCTION get_anzsic_division(INT) RETURNS VARCHAR(1) AS
'SELECT division FROM infoanzsic WHERE anzsic_id = \$1'
LANGUAGE SQL;

CREATE TABLE companies (
  company_id SERIAL PRIMARY KEY,
  company_name VARCHAR(128) NOT NULL UNIQUE,
  entity_id VARCHAR (3),
  anzsic_id INTEGER,
  anzsic_division VARCHAR(1),
  employee_count INTEGER,
  version INTEGER NOT NULL DEFAULT 0,
  updated_at TIMESTAMP DEFAULT current_timestamp,
  FOREIGN KEY (entity_id) REFERENCES infoabrentities(entity_id) ON DELETE RESTRICT,
  FOREIGN KEY (anzsic_id) REFERENCES infoanzsic(anzsic_id) ON DELETE RESTRICT,
  FOREIGN KEY (anzsic_division) REFERENCES infoanzsicdivision(division_id) ON DELETE RESTRICT,
  CHECK ( (anzsic_id IS NULL AND anzsic_division IS NULL) OR (anzsic_division = get_anzsic_division(anzsic_id)) ),
  CHECK ( employee_count > 0 )
);

CREATE TRIGGER update_companies_time
BEFORE UPDATE ON companies
FOR EACH ROW EXECUTE PROCEDURE update_modified_column_time();

CREATE TABLE companystates (
  company_name VARCHAR(128) NOT NULL,
  state_code VARCHAR(8) NOT NULL,
  PRIMARY KEY (company_name, state_code),
  FOREIGN KEY (company_name) REFERENCES companies(company_name) ON DELETE CASCADE,
  FOREIGN KEY (state_code) REFERENCES infostates(state_code) ON DELETE RESTRICT
);

CREATE TABLE infowagetypes (
  wagetype_id integer NOT NULL UNIQUE PRIMARY KEY,
  title VARCHAR (32) NOT NULL
);

CREATE TABLE infojobsite (
  jobsite_id integer NOT NULL UNIQUE PRIMARY KEY,
  title VARCHAR (32) NOT NULL
);

CREATE TABLE infoeducationlevel (
  edulevel_id integer NOT NULL UNIQUE PRIMARY KEY,
  title VARCHAR (16) NOT NULL
);

CREATE TABLE infojobtype (
  jobtype_id integer NOT NULL UNIQUE PRIMARY KEY,
  jobtype VARCHAR (32) NOT NULL
);

CREATE TABLE infoanzsco2021 (
  anzsco_id integer NOT NULL UNIQUE PRIMARY KEY,
  title VARCHAR (128) NOT NULL
);

CREATE TABLE phrasetypes (
  phrasetype_id SERIAL PRIMARY KEY,
  phrasetype VARCHAR (32) NOT NULL UNIQUE
);

CREATE TABLE phrases (
  phrase_id SERIAL PRIMARY KEY,
  phrasetype_id INTEGER NOT NULL,
  phrase_name VARCHAR (128) NOT NULL,
  UNIQUE (phrasetype_id, phrase_name),
  FOREIGN KEY (phrasetype_id) REFERENCES phrasetypes(phrasetype_id) ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS jobs (
  job_id SERIAL PRIMARY KEY,
  job_web_id VARCHAR (64) NOT NULL,
  jobsite_id INTEGER NOT NULL,
  data JSONB NOT NULL,
  UNIQUE (job_web_id, jobsite_id),
  FOREIGN KEY (jobsite_id) REFERENCES infojobsite(jobsite_id) ON DELETE RESTRICT
);

CREATE TABLE jobgroups (
  group_id SERIAL PRIMARY KEY,
  company_id INTEGER NOT NULL,
  version INTEGER NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  FOREIGN KEY (company_id) REFERENCES companies(company_id) ON DELETE RESTRICT
);

CREATE TABLE jobgrouplist (
  group_id INTEGER NOT NULL,
  job_id INTEGER NOT NULL,
  PRIMARY KEY (group_id, job_id),
  FOREIGN KEY (group_id) REFERENCES jobgroups(group_id) ON DELETE CASCADE,
  FOREIGN KEY (job_id) REFERENCES jobs(job_id) ON DELETE RESTRICT
);

CREATE TABLE jobdatum (
  group_id INTEGER UNIQUE NOT NULL PRIMARY KEY,
  anzsco_id integer NOT NULL DEFAULT 099888,
  jobtype_id integer NOT NULL DEFAULT 5,
  edulevel_id integer NOT NULL DEFAULT 3,
  date_listed DATE,
  state_code VARCHAR(8),
  age_min INTEGER,
  age_max INTEGER,
  version integer NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  FOREIGN KEY (group_id) REFERENCES jobgroups(group_id) ON DELETE CASCADE,
  FOREIGN KEY (anzsco_id) REFERENCES infoanzsco2021(anzsco_id) ON DELETE RESTRICT,
  FOREIGN KEY (jobtype_id) REFERENCES infojobtype(jobtype_id) ON DELETE RESTRICT,
  FOREIGN KEY (edulevel_id) REFERENCES infoeducationlevel(edulevel_id) ON DELETE RESTRICT,
  FOREIGN KEY (state_code) REFERENCES infostates(state_code) ON DELETE RESTRICT
);

CREATE TABLE jobwages (
  group_id INTEGER UNIQUE NOT NULL PRIMARY KEY,
  wagetype_id integer,
  low_wage integer CHECK (low_wage IS NULL OR high_wage IS NOT NULL),
  high_wage integer CHECK (low_wage is NULL OR high_wage >= low_wage),
  FOREIGN KEY (group_id) REFERENCES jobdatum(group_id) ON DELETE CASCADE,
  FOREIGN KEY (wagetype_id) references infowagetypes(wagetype_id) ON DELETE RESTRICT
);

CREATE TABLE jobexperience (
  group_id INTEGER UNIQUE NOT NULL PRIMARY KEY,
  years_needed NUMERIC (2) NOT NULL,
  FOREIGN KEY (group_id) REFERENCES jobdatum(group_id) ON DELETE CASCADE
);

CREATE TABLE jobphrases (
  group_id INTEGER NOT NULL,
  phrase_id INTEGER NOT NULL,
  value REAL NOT NULL,
  FOREIGN KEY (phrase_id) REFERENCES phrases(phrase_id) ON DELETE CASCADE,
  FOREIGN KEY (group_id) REFERENCES jobdatum(group_id) ON DELETE CASCADE,
  UNIQUE (group_id, phrase_id)
);

EOSQL

echo ">> Copying CSV to database..."
for file in `ls -1 ${CSV_FILES_DIR}`
do
  echo "Copying file ${file}"
  PGPASSWORD=dummyPass psql -U postgres -d postgres -h "${POSTGRES_HOST}" -c "\COPY ${file:2:-4}($(head -n 1 ${CSV_FILES_DIR}/${file})) FROM ${CSV_FILES_DIR}/${file} WITH(DELIMITER ',', NULL 'NULL', FORMAT csv, HEADER true)";
done

echo ">> Creating service account for Grafana..."
PGPASSWORD=${POSTGRES_PASSWORD} psql -U postgres -d postgres -h "${POSTGRES_HOST}" <<-EOSQL

DROP OWNED BY svc_grafana;
DROP USER IF EXISTS svc_grafana;

CREATE USER svc_grafana WITH PASSWORD '${POSTGRES_SVC_GRAFANA_PASSWORD}';
GRANT USAGE ON SCHEMA public TO svc_grafana;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO svc_grafana;

EOSQL

echo ">> Loading SQL scripts from directory ${SQL_SCRIPTS_DIR}..."
for file in `ls -1 ${SQL_SCRIPTS_DIR}/*.sql`
do
  echo "Executing script ${file}"
  PGPASSWORD=dummyPass psql -U postgres -d postgres -h "${POSTGRES_HOST}" -a -f ${file}
done

echo ">> Database init complete!"
